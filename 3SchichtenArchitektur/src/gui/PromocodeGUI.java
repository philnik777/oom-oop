package gui;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import logic.IPromoCodeLogic;

public class PromocodeGUI extends JFrame
{
	private JPanel contentPane;
	private JMenuBar menuBar;
	private JMenu menu;
	private JMenuItem menuItem;
	private PromoCodeShow promoCodeShow = new PromoCodeShow();
	private final JLabel TENBUSCH_ERROR = new JLabel("<html>Das ist nicht Teil der Aufgabe. " +
			"<br>Bei Beschwerden bitte an: " +
			"Tenbusch@oszimt.de</html>");
	private final Color TEN_COLOR = new Color(10, 10, 10);
	private final Color ANTI_TEN_COLOR = new Color(245, 245, 245);

	public PromocodeGUI(IPromoCodeLogic logic)
	{
		setTitle("PromoCode Generator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 250);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.setBackground(TEN_COLOR);
		promoCodeShow.labelBeschriftungPromoCode = new JLabel("Willkommen zum PromoCode Generator!");
		promoCodeShow.labelBeschriftungPromoCode.setHorizontalAlignment(JLabel.CENTER);
		contentPane.add(promoCodeShow.labelBeschriftungPromoCode, BorderLayout.CENTER);
		this.menuBar = new JMenuBar();

		menuBar.setBackground(ANTI_TEN_COLOR);
		setJMenuBar(menuBar);
		this.menu = new JMenu("Edit");
		this.menuBar.add(menu);
		this.menuItem = new JMenuItem("PromoCode Generator");
		this.menu.add(this.menuItem);
		this.menuItem.addActionListener(e ->
		{
			contentPane.remove(promoCodeShow.labelBeschriftungPromoCode);
			promoCodeShow = showingPromoCode(logic);
			promoCodeShow.labelBeschriftungPromoCode.setHorizontalAlignment(JLabel.CENTER);
			contentPane.add(promoCodeShow.labelPromoCode, BorderLayout.CENTER);
			contentPane.add(promoCodeShow.labelBeschriftungPromoCode, BorderLayout.NORTH);
			contentPane.add(promoCodeShow.buttonNewPromoCode, BorderLayout.SOUTH);
			contentPane.revalidate();
		});

		this.menu = new JMenu("Help");
		this.menuBar.add(menu);
		this.menuItem = new JMenuItem("Check for Updates");
		this.menu.add(menuItem);
		this.menuItem.addActionListener(e ->
		{
			contentPane.remove(promoCodeShow.labelBeschriftungPromoCode);
			TENBUSCH_ERROR.setHorizontalAlignment(JLabel.CENTER);
			promoCodeShow.labelBeschriftungPromoCode = TENBUSCH_ERROR;
			contentPane.add(promoCodeShow.labelBeschriftungPromoCode, BorderLayout.CENTER);
			contentPane.revalidate();
		});

		this.setVisible(true);

	}

	public static class PromoCodeShow
	{
		private JLabel labelPromoCode;
		private JButton buttonNewPromoCode;
		private JLabel labelBeschriftungPromoCode;
	}

	public PromoCodeShow showingPromoCode(IPromoCodeLogic logic)
	{
		PromoCodeShow promoCodeShow = new PromoCodeShow();

		promoCodeShow.labelPromoCode = new JLabel("");
		promoCodeShow.labelPromoCode.setHorizontalAlignment(SwingConstants.CENTER);
		promoCodeShow.labelPromoCode.setFont(new Font("Arial", Font.BOLD, 15));

		promoCodeShow.buttonNewPromoCode = new JButton("Generiere neuen PromoCode");
		promoCodeShow.buttonNewPromoCode.addActionListener(actionEvent ->
		{
			String promoCode = logic.getNewPromoCode();
			promoCodeShow.labelPromoCode.setText(promoCode);
		});

		promoCodeShow.labelBeschriftungPromoCode = new JLabel("Der PromoCode verdoppelt die IT-Dollar");

		return promoCodeShow;
	}
}

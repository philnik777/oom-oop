package data;

import java.io.*;
import java.util.ArrayList;

public class PromoCodeData implements IPromoCodeData {

	private BufferedWriter outputWriter;
	private static final String filePath = "codes.txt";
	private ArrayList<String> promocodes = new ArrayList<>();

	private void loadPromoCodes() throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(this.filePath));
		String line = reader.readLine();
		while (line != null) {
			promocodes.add(line);
			line = reader.readLine();
		}
		reader.close();
	}

	public PromoCodeData() throws IOException {
		this.loadPromoCodes();
		this.outputWriter = new BufferedWriter(new FileWriter(this.filePath, true));
	}

	@Override
	public boolean savePromoCode(String code) {
		try {
			this.outputWriter.write(code + "\n");
			this.outputWriter.flush();
			this.promocodes.add(code);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean isPromoCode(String code) {
		return this.promocodes.contains(code);
	}

}

package start;

import gui.PromocodeGUI;
import gui.PromocodeTUI;
import logic.IPromoCodeLogic;
import logic.PromoCodeLogic;
import data.IPromoCodeData;
import data.PromoCodeData;

import java.io.IOException;

public class PromoCodeStart {

	public static void main(String[] args) {
		IPromoCodeData data = null;
		try {
			data = new PromoCodeData();
			IPromoCodeLogic logic = new PromoCodeLogic(data);

			new PromocodeGUI(logic);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}

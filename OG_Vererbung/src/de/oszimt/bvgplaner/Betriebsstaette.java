package de.oszimt.bvgplaner;

public class Betriebsstaette extends Haltepunkt {
	
	// Attribute
	private int anzahlFahrzeuge;
	
	// Konstruktor
	public Betriebsstaette()
	{
	}

	public int getAnzahlFahrzeuge() {
		return anzahlFahrzeuge;
	}

	public void setAnzahlFahrzeuge(int anzahlFahrzeuge) {
		this.anzahlFahrzeuge = anzahlFahrzeuge;
	}

	
	
}
